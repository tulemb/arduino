# ארדואינו למתחילים



## קישורים להרצאות



[שיעור 2](https://docs.google.com/document/d/1rb0WR5M5UJ8kTFan6pUbTtLg0PcwzXXN7M5m8aq6Fpg/edit?usp=sharing) - מבוא קצר לתכנות

[שיעור 3](https://docs.google.com/presentation/d/1yG_nVdX6QsV0_X6KazvJJyLrzg1zzC-wXEPHxbF7BRA/edit?usp=sharing) - OUTPUT

[שיעור 4](https://docs.google.com/presentation/d/17tzHdZEC23V4HM8HOWZ-DtQ_b2KPvN8WCM7dWy4gk_U/edit?usp=sharing) - INPUT

[שיעור 5](https://docs.google.com/presentation/d/1hVRHLzSAq5S0zrQaw2SBklBgxTfJm45T12eFOtEI-4Q/edit?usp=sharing) - ANALOG

[שיעור 6](https://docs.google.com/presentation/d/1kQbkTjpVkwHP5x3tfWtFRUe0Ss-ugvb4JN0Ja7jRjZY/edit?usp=sharing) - Function

[שיעור 7](https://docs.google.com/presentation/d/1zzfM3R8BIP_YSRP07x01DebXGjMw-l5O2bo-Dm6WeMo/edit?usp=sharing) - Joystick

[שיעור 8](https://docs.google.com/presentation/d/1khJlr5TlEymI0BmojAOZA-xJQ5J2WnzCIt61MKgwc70/edit?usp=sharing) - תרגילים

[שיעור 9](https://docs.google.com/presentation/d/10l99QG7yUtGic_vcv1IjdmQFkaQLqz59SMKcMnrIMik/edit?usp=sharing) - מנועה סרבו

[שיעור 10](https://docs.google.com/presentation/d/1TtjzmOkkpNqtEb97TSBCHgxKC9BXA2TnOZLbwWul_E8/edit?usp=sharing) - LCD

[שעור 11](https://docs.google.com/presentation/d/1-PD77jD6aRYEQpdFR3UjqBcNV_DeGHb6UkTt5Elj45I/edit?usp=sharing) - סיכום


## עזרים

[ דף נוסחאות של קוד](https://www.arduino.cc/reference/en/)

[ דף נוסחאות שלי](https://docs.google.com/document/d/1h61PDFWwFV1pyPEMsiQi1zWZhyT3nEIyPEsrWzvKw0g/edit?usp=sharing)

[א tinkercad](https://www.tinkercad.com/) - סימולציה של ארדואינו עם קוד ורכיבים

## קניית רכיבים

[ארדואינו גרסה מקורית](https://he.aliexpress.com/item/Smart-Electronics-UNO-R3-MEGA328P-ATMEGA16U2-Development-Board-Without-USB-Cable-for-arduino-Diy-Starter-Kit/32832528573.html?spm=a2g0s.9042311.0.0.2b524c4d6xw2AM)

[ערכת מתחילים עם ארדואינו](https://he.aliexpress.com/item/NEWEST-RFID-Starter-Kit-for-Arduino-UNO-R3-Upgraded-version-Learning-Suite-With-Retail-Box/32714696336.html?spm=a2g0s.9042311.0.0.27424c4dr5zGLR)


## פרטי קשר

טובי למברג

מייל: tulemb@gmail.com




