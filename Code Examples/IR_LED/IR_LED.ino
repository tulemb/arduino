#include <IRremote.h>                                           // מוסיף את ספריית הפקודות של הא"א
int RECV_PIN = 2;                                               // הצהרת משתנים
int LED_PIN = 3;                                                // הצהרת משתנים
IRrecv irrecv(RECV_PIN);                                        // הגדרת רגל 2 כרגל המקלט
decode_results results;                                         // הגדרת ספריית הפלט

void setup() {                
  Serial.begin(9600);                                           // קביעת קצב התקשורת עם הארדואינו
  pinMode(RECV_PIN, INPUT);                                     // הגדרת רגל המקלט ככניסה
  pinMode(LED_PIN, OUTPUT);                                     // הגדרת רגל הלד כיציאה
  irrecv.enableIRIn();                                          // התחל קליטה
}
void loop() {
   if (irrecv.decode(&results)) {                               // אם נקלטה לחיצה על השלט
     Serial.println(results.value, HEX);                        // הדפס את האות על המסך
     irrecv.resume();                                           // המשך קליטה               
    if (results.value == 0xFF02FD ) {digitalWrite(3,LOW);}      // אם האות תואם לכפתור הכיבוי- כבה את הלד
    if (results.value == 0xFF22DD ) {digitalWrite(3,HIGH);}     // אם האות תואם לכפתור ההדלקה- הדלק את הלד
   }   
 }
