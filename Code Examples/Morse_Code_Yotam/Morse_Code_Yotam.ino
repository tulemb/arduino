char ch;
int ledPin = 13;
void setup() {
  // put your setup code here, to run once:
  pinMode(13, OUTPUT);
  Serial.begin(9600);
}
void flash(int t )
{
  digitalWrite(ledPin, HIGH);
  delay(t);
  digitalWrite(ledPin, LOW);
  delay(200);
}


void loop() {
  ch = Serial.read();
  if (ch == 'a') {
    flash(200); flash(500);
  }
  if (ch == 'b') {
    flash(500); flash(200); flash(200); flash(200);
  }
  if (ch == 'c') {
    flash(500); flash(200); flash(500); flash(200);
  }
  if (ch == 'd') {
    flash(500); flash(200); flash(200);
  }

  if (ch == 'e') {
    flash(200);
  }
  if (ch == 'f') {
    flash(200); flash(200); flash(500); flash(200);
  }
  if (ch == 'g') {
    flash(500); flash(500); flash(200);
  }
  if (ch == 'h') {
    flash(200); flash(200); flash(200); flash(200);
  }
  if (ch == 'i') {
    flash(200); flash(200);
  }
  if (ch == 'j') {
    flash(200); flash(500); flash(500); flash(500);
  }
  if (ch == 'k') {
    flash(500); flash(200); flash(500);
  }
  if (ch == 'l') {
    flash(200); flash(500); flash(200); flash(200);
  }
  if (ch == 'm') {
    flash(500); flash(500);
  }
  if (ch == 'n') {
    flash(500); flash(200);
  }
  if (ch == 'o') {
    flash(500); flash(500); flash(500);
  }
  if (ch == 'p') {
    flash(200); flash(500); flash(500); flash(200);
  }
  if (ch == 'q') {
    flash(500); flash(500); flash(200); flash(500);
  }
  if (ch == 'r') {
    flash(200); flash(500); flash(200);
  }
  if (ch == 's') {
    flash(200); flash(200); flash(200);
  }
  if (ch == 't') {
    flash(500);
  }
  if (ch == 'u') {
    flash(200); flash(200); flash(500);
  }
  if (ch == 'v') {
    flash(200); flash(200); flash(200); flash(500);
  }
  if (ch == 'w') {
    flash(200); flash(500); flash(500);
  }
  if (ch == 'x') {
    flash(500); flash(200); flash(200); flash(500);
  }
  if (ch == 'y') {
    flash(500); flash(200); flash(500); flash(500);
  }
  if (ch == 'z') {
    flash(500); flash(500); flash(200); flash(200);
  }







}
