/*********************************************
author: Yotam
Assistent: Tal
*********************************************/



#include <LiquidCrystal.h>
const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);
void setup() {
  Serial.begin(9600);
  lcd.begin(16,2);
  // set up the LCD's number of columns and rows:
 
}
String removeEnter(String &str)
{
  for(char& ch : str)
  {
    if (ch == '\n')
    {
      ch = ' ';
    }
  }
  return str;
}
void loop() {
  String st = Serial.readString();
  st.concat("\0");
  Serial.println(st);
  lcd.print(removeEnter(st));
  delay(5000);
  lcd.clear();
  // set the cursor to column 0, line 1
  // (note: line 1 is the second row, since counting begins with 0):
}
