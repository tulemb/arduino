const int buzzer = 9;
const int ledPin = 13;
void setup() {
  // put your setup code here, to run once:
  pinMode(buzzer, OUTPUT);
  pinMode(ledPin, OUTPUT);
}

void loop()
{
  flash(200); flash(200); flash(200);  // S ...
  flash(400); flash(400); flash(400); // O ---
  flash(200); flash(200); flash(200);  // S …
  delay(200);
}


void flash(int t)
{
  digitalWrite(ledPin, HIGH);
  tone(buzzer, 1000);
  delay(t);
  digitalWrite(ledPin, LOW);
  noTone(buzzer);
  delay(200);
}
