int VRX = A1;
int VRY = A0;
int x_in, x_out;
int y_in, y_out;
int serbo1 = 6;
int serbo2 = 9;

void setup() {
  pinMode(serbo1, OUTPUT);
  pinMode(serbo2, OUTPUT);
  Serial.begin(9600);
}

int imput_to_output(int in)
{
  return (in * 255.) / 1023.;
}

void loop() {
  x_in = analogRead(VRX);
  y_in = analogRead(VRY);

  x_out = imput_to_output(x_in);
  y_out = imput_to_output(y_in);
  
  analogWrite(serbo1, x_out);
  analogWrite(serbo2, y_out);
  
  Serial.print("X out");
  Serial.print(x_out);
  Serial.print(" Y out");
  Serial.println(y_out);
  delay(50);
}
