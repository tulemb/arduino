#include <IRremote.h>
#include <Servo.h>

Servo servo0;
Servo servo1;
int ang0 = 90;
int ang1 = 90;
int astep = 50;
const int RECV_PIN = 7;
IRrecv irrecv(RECV_PIN);
decode_results results;

void setup() {
  servo0.attach(6);
  servo1.attach(9);
  Serial.begin(9600);
  irrecv.enableIRIn();
}

void loop() {
  if (irrecv.decode(&results)) {
    Serial.println(results.value, HEX);

    switch (results.value)
    {
      case 0xFF18E7: // 2 up
        ang0 = ang0 + astep < 180 ? ang0 + astep : 180;
        break;

      case 0xFF10EF: // 4 left
        ang1 = ang1 - astep > 0 ? ang1 - astep : 0;
        break;

      case 0xFF5AA5: // 6 right
        ang1 = ang1 + astep < 180 ? ang1 + astep : 180;
        break;

      case 0xFF4AB5: // 8 down
        ang0 = ang0 - astep > 0 ? ang0 - astep : 0;
        break;
    }
    irrecv.resume();

    Serial.print("ang1 ");
    Serial.print(ang1);
    Serial.print(" ang0 ");
    Serial.println(ang0);
  }
  servo0.write(ang0);
  servo1.write(ang1);
  delay(50);
}
